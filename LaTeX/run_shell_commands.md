# Run shell commands inside LaTeX

This is useful for example for including content (images, ...) from the web:
```tex
\immediate\write18{wget http://file/from/the/net}
```

To avoid redownloading files each time:
```tex
\IfFileExists{filename}{true-branch}{false-branch}
```

### Reference

https://tex.stackexchange.com/questions/5433/can-i-use-an-image-located-on-the-web-in-a-latex-document
https://tex.stackexchange.com/questions/98203/can-i-test-if-a-file-exists
