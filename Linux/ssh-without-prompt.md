# SSH Connection Without Prompting

When connecting to a remote via SSH (`ssh user@remote`), it prompts for the
`user`'s credentials.
To bypass it, you need to create a local SSH key and then export it to the
remote.
The key should be passwordless.

```
[user1@local]$ ssh-keygen
Generating public/private rsa key pair.
...  # Hit enter at password prompt
[user1@local]$ ssh-copy-id user2@remote
user1@remote's password:
[user1@local]$ ssh user2@remote
Last login: ...
[user2@remote]$ hostname
remote
```

## Links

https://www.linuxtrainingacademy.com/ssh-login-without-password/