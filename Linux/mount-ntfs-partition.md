# Mount NTFS partition

Running
```
sudo mount -t ntfs -o nls=utf8,umask=0222 /dev/sda6 ~/mnt
```
mounts NTFS (Windows) partition `/dev/sda6` to `~/mnt`.

Partitions can be explored i.e. by running `lsblk`.