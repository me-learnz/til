# Trap .bash_logout

`.bash_logout` is not called when a terminal window is closed abruptly (i.e. not with exit/logout command or Ctrl-D).
It is possible to run a command upon a non-standard termination using `trap`.
For example:

```bash
print_goodbye () { echo Goodbye; }
trap print_goodbye EXIT
```

This can be defined as a global function (e.g. in `.bash_profile`).
You can use it to call the `.bash_logout`.

### Reference

https://superuser.com/a/410534