# Improve makepkg compile times on Arch Linux

* Uncomment and set the `MAKEPKG` env variable in `/etc/makepkg.conf`, e.g.: `MAKEPKG="-j$(nproc)"`.
* Use `tmpfs` for a working directory which is in memory => faster  (`BUILDDIR=/tmp/makepkg`)

Source: https://wiki.archlinux.org/index.php/Makepkg#Improving_compile_times