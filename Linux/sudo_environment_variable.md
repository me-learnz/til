# Environment variables when running `sudo`

When running `sudo <cmd>`, the environment variables visible to user are not visible for the `<cmd>`.
There are few options:
* Run with `sudo -E`.
* Add the variables to sudoers:
    - `sudo visudo`
    - Add this: `Defaults env_keep += "ENV1 ENV2 ..."`
    
## Links 
https://stackoverflow.com/questions/8633461/how-to-keep-environment-variables-when-using-sudo
