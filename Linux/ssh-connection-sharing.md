# SSH connection sharing 

After running
```
ssh -MN user@remote
```
it's not necessary to login again in following sessions (as long as the above is 
running).