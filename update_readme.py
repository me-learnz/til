"""
Update sections in README.md

Automatically create an update these

* Pinned
* Newest TILs
* List of all TILs

Run from repository root.

For newest tils:
git log --pretty=format: --name-only

Line numbering from 0 everywhere.
"""

import os
import subprocess

from pathlib import Path


only_suffixes = [".md"]
ignore_dirs = [".git", "__pycache__"]
ignore_files = ["README.md", "update_readme.py"]
repo_link = "https://gitlab.com/me-learnz/til/tree/master"
readme_file = "README.md"
newest_tils_to_list = 10

def mdtag(s):
	return "[//]: # ({})".format(s)


def subdirs(start_path, depth=-1, ignorelist=[]):
	"""Yields all subdirectories of 'start_path' to a depth specified by
	'depth'.

	Ignores directory names in ignorelist.

	Args:
	start_path (pathlib.Path): Starting directory path.
		depth (int): How many levels to descend. -1 descends all.
		ignorelist (list of str): Directories with names in ignorelist are not
		  yielded.

	Yields:
		pathlib.Path: Path object of the subdirectory.
	"""

	for child in start_path.iterdir():
		if all([child.is_dir(), child.name not in ignorelist]):
			if depth == 0:
				break
			else:
				yield child
				yield from subdirs(child, depth-1)


def subdirs_top(start_path, depth=-1, ignorelist=[]):
	"""Same as subdirs but includes 'start_path'.
	"""

	yield start_path
	for subdir in subdirs(start_path, depth, ignorelist):
		yield subdir


def all_tils(path, dirs_to_ignore, files_to_ignore, file_suffixes):
	if not isinstance(path, Path):
		path = Path(path)
	til_list = []
	for subdir in subdirs_top(path, ignorelist=dirs_to_ignore):
		for f in os.listdir(subdir):
			file_path = Path(subdir).joinpath(f)
			if file_path.is_file() and (f not in files_to_ignore):
				if any([f.endswith(s) for s in file_suffixes]):
					til_list.append(file_path)
	return til_list


def link_from_path(link_name, path, link_files=True, link_dirs=False):
	if (link_files and path.is_file()) or (link_dirs and path.is_dir()):
		return "[{}]({}/{})".format(link_name, repo_link, '/'.join(path.parts))
	else:
		return str(link_name)


def til_header(path):
	with open(path, 'r') as rf:
		for line in rf:
			line = line.strip()
			if line.startswith('#'):
				return line.strip('#').strip()


def paths_to_tree(paths, link_files=True, link_dirs=False, use_headers=True):
	"""'use_header' doesn't work if target file is til file.
	"""

	tree = {}
	for path in paths:
		current_tree = tree
		path_steps = Path("")
		for part in path.parts:
			path_steps = path_steps.joinpath(part)
			if use_headers and path_steps.is_file():
				part = til_header(path_steps)
			tree_node = link_from_path(part, path_steps, link_files, link_dirs)
			if tree_node not in current_tree:
				current_tree[tree_node] = {}
			current_tree = current_tree[tree_node]
	return tree


def til_tree_strings(til_tree, indent_level=0, indent_str="\t"):
	for node, subtree in til_tree.items():
		yield "{}* {}\n".format(indent_level*indent_str, node)
		if not subtree == {}:
			yield from til_tree_strings(subtree, indent_level+1, indent_str)


def search_in_file(s, filename):
	occurences = []
	with open(filename, 'r') as rf:
		lines = rf.readlines()
		for i, line in enumerate(lines):
			if s in line:
				occurences.append(i)
	return occurences


def filter_pinned(til_list, pinned_tag_name='pinned'):
	filtered_tils = []
	for til in til_list:
		if search_in_file(mdtag(pinned_tag_name), til):
			filtered_tils.append(til)
	return filtered_tils


def filter_new(til_list, new_cnt=10):
	"""Works only for committed tils.
	"""

	cmd = "git log --pretty=format: --name-only"
	args = cmd.split(' ')

	out = subprocess.run(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	if out.returncode:
		raise OSError("{} failed with return code {}."
		                .format(out.stderr, out.returncode))

	stdout = out.stdout.decode("utf-8").split('\n')
	file_list = [s for s in stdout if s]  # Filter out empty strings

	newest_files = []
	cnt = new_cnt
	for f in file_list:
		if cnt <= 0:
			break
		fp = Path(f)
		if (fp not in newest_files) and (fp in til_list):
			newest_files.append(fp)
			cnt -= 1

	return newest_files


def print_tils(til_list, target_file, tag_name,
	           mode='header', indent_level=0, indent_str='\t'):
	"""Print tils in 'til_list' under the 'tag_name' in 'target_file'.
	"""


	tag_start = mdtag(tag_name)
	tag_end = mdtag(tag_name + "_end")

	tag_start_lines = search_in_file(tag_start, target_file)
	tag_end_lines = search_in_file(tag_end, target_file)

	if not tag_start_lines:
		raise ValueError("Tag {} not present in file {}."
		                   .format(tag_start, target_file))
	if not tag_end_lines:
		raise ValueError("Tag {} not present in file {}."
		                   .format(tag_end, target_file))
	if (tag_end_lines[0] - tag_start_lines[0]) <= 0:
		raise ValueError("{} and {} are in wrong order in file {}."
		                   .format(tag_start, tag_end, target_file))

	with open(target_file, 'r') as rf:
		lines = rf.readlines()
		before_tag = lines[:tag_start_lines[0]]
		after_tag = lines[tag_end_lines[0]+1:]

	headers = []
	for til in til_list:
		headers.append(til_header(til))

	with open(target_file, 'w') as wf:
		for line in before_tag:
			wf.write(line)
		wf.write(tag_start + '\n')
		if mode == 'tree':
			til_tree = paths_to_tree(til_list)
			for line in til_tree_strings(til_tree, indent_level, indent_str):
				wf.write(line)
		else:
			for til, header in zip(til_list, headers):
				link = link_from_path(header, til)
				wf.write("{}* {}\n".format(indent_level*indent_str, link))

		wf.write(tag_end + '\n')
		for line in after_tag:
			wf.write(line)


def try_commit_push():
	add_cmd    = 'git add README.md'
	commit_cmd = 'git commit -m'
	push_cmd   = 'git push origin master'

	subprocess.run(add_cmd.split())
	subprocess.run(commit_cmd.split() + ["Automatic README.md update"])
	subprocess.run(push_cmd.split())


def main():
	til_list_all = all_tils('.', ignore_dirs, ignore_files, only_suffixes)

	print_tils(til_list_all, readme_file, "all_tils", mode='tree')

	pinned_list = filter_pinned(til_list_all)
	new_list = filter_new(til_list_all, newest_tils_to_list)

	print_tils(pinned_list, readme_file, "pinned", mode='header')
	print_tils(new_list, readme_file, "newest", mode='header')

	try_commit_push()

if __name__ == "__main__":
	main()
