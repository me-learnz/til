# ![](https://github.com/dcurtis/markdown-mark/blob/master/png/32x20.png?raw=true) Markdown Mark

A mark to mark your Markdown.
Oh... 

Use it to indicate Markdown support.
Can be approximated with [M↓].

### Links
* GitHub: https://github.com/dcurtis/markdown-mark
* blog post: https://dcurt.is/the-markdown-mark