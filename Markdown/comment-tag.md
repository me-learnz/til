# Comment Tag

I use this syntax to mark points in a Markdown document.
It can also be used for commenting.

### Syntax
```
[//]: # (this text won't be included even in the resulting HTML) 
```

### Links
* StackOverflow answer: https://stackoverflow.com/a/20885980
