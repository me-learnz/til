# Today I Learned (TIL)

Random small things I learn along the way which are normally scattered between my brain and million bookmarks.

[![pipeline status](https://gitlab.com/me-learnz/til/badges/master/pipeline.svg)](https://gitlab.com/me-learnz/til/pipelines)


## Contents

* [**Pinned tils**](#pinned-tils)
* [**Newest tils**](#newest-tils)
* [**List of all tils**](#list-of-all-tils)
* [**Credits**](#credits)

## Pinned tils
[//]: # (pinned)
* [Introduction to GitLab CI](https://gitlab.com/me-learnz/til/tree/master/git/gitlab-ci-intro.md)
* [RegexOne - Interactive regex tutorial](https://gitlab.com/me-learnz/til/tree/master/regex/regexone-tutorial.md)
[//]: # (pinned_end)

## Newest tils
[//]: # (newest)
* [explainshell.com](https://gitlab.com/me-learnz/til/tree/master/Linux/explainshell.md)
* [Improve makepkg compile times on Arch Linux](https://gitlab.com/me-learnz/til/tree/master/Linux/arch-faster-makepkg.md)
* [SSH connection sharing](https://gitlab.com/me-learnz/til/tree/master/Linux/ssh-connection-sharing.md)
* [Run shell commands inside LaTeX](https://gitlab.com/me-learnz/til/tree/master/LaTeX/run_shell_commands.md)
* [Trap .bash_logout](https://gitlab.com/me-learnz/til/tree/master/Linux/trap_logout_script.md)
* [Table of Expressions for `if`](https://gitlab.com/me-learnz/til/tree/master/bash/if-expressions.md)
* [Preserve aliases on `sudo`](https://gitlab.com/me-learnz/til/tree/master/bash/sudo-aliases.md)
* [`cd` to home](https://gitlab.com/me-learnz/til/tree/master/Linux/cd-home.md)
* [SSH Connection Without Prompting](https://gitlab.com/me-learnz/til/tree/master/Linux/ssh-without-prompt.md)
* [Environment variables when running `sudo`](https://gitlab.com/me-learnz/til/tree/master/Linux/sudo_environment_variable.md)
[//]: # (newest_end)

## List of all tils
[//]: # (all_tils)
* LaTeX
	* [Run shell commands inside LaTeX](https://gitlab.com/me-learnz/til/tree/master/LaTeX/run_shell_commands.md)
* Python
	* [Decorators Explained](https://gitlab.com/me-learnz/til/tree/master/Python/decorators.md)
* Linux
	* [Trap .bash_logout](https://gitlab.com/me-learnz/til/tree/master/Linux/trap_logout_script.md)
	* [Improve makepkg compile times on Arch Linux](https://gitlab.com/me-learnz/til/tree/master/Linux/arch-faster-makepkg.md)
	* [Mount NTFS partition](https://gitlab.com/me-learnz/til/tree/master/Linux/mount-ntfs-partition.md)
	* [explainshell.com](https://gitlab.com/me-learnz/til/tree/master/Linux/explainshell.md)
	* [Environment variables when running `sudo`](https://gitlab.com/me-learnz/til/tree/master/Linux/sudo_environment_variable.md)
	* [`cd` to home](https://gitlab.com/me-learnz/til/tree/master/Linux/cd-home.md)
	* [SSH connection sharing](https://gitlab.com/me-learnz/til/tree/master/Linux/ssh-connection-sharing.md)
	* [SSH Connection Without Prompting](https://gitlab.com/me-learnz/til/tree/master/Linux/ssh-without-prompt.md)
* git
	* [Introduction to GitLab CI](https://gitlab.com/me-learnz/til/tree/master/git/gitlab-ci-intro.md)
* regex
	* [RegexOne - Interactive regex tutorial](https://gitlab.com/me-learnz/til/tree/master/regex/regexone-tutorial.md)
	* [RegExr, online regex parser](https://gitlab.com/me-learnz/til/tree/master/regex/regexr.md)
* bash
	* [Auto-filling Prompts](https://gitlab.com/me-learnz/til/tree/master/bash/autofill-prompts.md)
	* [Preserve aliases on `sudo`](https://gitlab.com/me-learnz/til/tree/master/bash/sudo-aliases.md)
	* [Table of Expressions for `if`](https://gitlab.com/me-learnz/til/tree/master/bash/if-expressions.md)
	* [Redirecting terminal output to file](https://gitlab.com/me-learnz/til/tree/master/bash/terminal-output-to-file.md)
* Markdown
	* [Babelmark 2](https://gitlab.com/me-learnz/til/tree/master/Markdown/babelmark-2.md)
	* [![](https://github.com/dcurtis/markdown-mark/blob/master/png/32x20.png?raw=true) Markdown Mark](https://gitlab.com/me-learnz/til/tree/master/Markdown/markdown-mark.md)
	* [Comment Tag](https://gitlab.com/me-learnz/til/tree/master/Markdown/comment-tag.md)
[//]: # (all_tils_end)

## Credits

Inspired by [jbranchaud/til](https://github.com/jbranchaud/til) who [shamelessly stole](https://github.com/jbranchaud/til#about) the idea from [thoughtbot/til](https://github.com/thoughtbot/til).