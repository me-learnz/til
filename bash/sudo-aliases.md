# Preserve aliases on `sudo`

Add this to `.bashrc` (note the space):
```bash
alias sudo='sudo '
```

## Links

https://unix.stackexchange.com/a/349290/270982