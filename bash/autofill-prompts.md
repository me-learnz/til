# Auto-filling Prompts

When running e.g. Continuous Integration scripts, it might be necessary to fill answers to script prompts.

Several options (extracted from [[1]](https://stackoverflow.com/questions/3804577/have-bash-script-answer-interactive-prompts)):

* `echo "Y Y N N Y N Y Y N" | ./your_script`
* `printf 'y\ny\ny\n' | ./your_script`
* `cat "input.txt" | ./Script.sh`
* `yes Y |./your_script` or `yes N |./your_script`
* [expect](http://expect.sourceforge.net) tool


## Links

[1] https://stackoverflow.com/questions/3804577/have-bash-script-answer-interactive-prompts